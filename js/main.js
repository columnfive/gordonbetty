var firstSrcImageSize = "609x433_";
var secondSrcImageSize = "381x572_";
var thirdSrcImageSize = "465x495_";

var contentPrograms = {
	"programs" : [
		{
			"title"    : "Science Program",
			"subtitle" : "Emergent Phenomena in Quantum Systems and Data-Driven Discovery Investigators Announced",
			"icon"     : "Science-icon-gordon-and-betty.gif",
			"src1"     : firstSrcImageSize + "0007_1.png",
			"src3"     : secondSrcImageSize + "0007_1.png",
			"src4"     : thirdSrcImageSize + "0007_1.png",
			"impact"   : "The Moore Foundation selected 19 new Emergent Phenomena in Quantum Systems investigators to accelerate scientific breakthroughs in the synthesis, theory and characterization of quantum materials through a five-year, $34.2 million initiative. In addition, 14 Data-Driven Discovery investigators were selected to catalyze new types of knowledge and advance new data science methods across a wide spectrum of disciplines through a five-year, $21 million initiative.",
			"grantees" : "<i><a target='_blank' href=\'https://www.moore.org/article-detail?newsUrlName=gordon-and-betty-moore-foundation-selects-new-awardees-in-experimental-physics-to-stimulate-discoveries-in-quantum-materials\'>EPiQS Investigators</a></i> and <i><a target='_blank' href=\'https://www.moore.org/article-detail?newsUrlName=the-gordon-and-betty-moore-foundation-selects-awardees-for-$21-million-in-grants-to-stimulate-data-driven-discovery\'>DDD Investigators </a></i>",
			"moreLink" : "https://www.moore.org/programs/science/emergent-phenomena-in-quantum-systems",
			"linkText" : "See more about our Science Program here.",
			"mainLink" : "http://www.moore.org/programs/science",
			"courtesy" : "Courtesy of IDEO"
		},
		{
			"title"    : "Patient Care Program",
			"subtitle" : "Patient and Family Engagement Efforts See Creation of RoadMap in Practice and Research",
			"icon"     : "Medical-gordon-and-betty-moore-icon.gif",
			"src1"     : firstSrcImageSize + "0006_2.png",
			"src3"     : secondSrcImageSize + "0006_2.png" ,
			"src4"     : thirdSrcImageSize + "0006_2.png",
			"impact"   : "Engaging patients and their families in how care is delivered is essential to changing our health care system for the better. Our efforts focused on paving the way for broad adoption of meaningful patient and family engagement. In partnership with the American Institutes for Research, we brought together advocates, thought leaders and practitioners to co-create a roadmap for how to work with patients and families in the delivery of health care.",
			"grantees" : "<i><a target='_blank' href='http://www.air.org/'>American Institutes for Research</a></i>",
			"moreLink" : "http://patientfamilyengagement.org/",
			"linkText" : "See more about our Patient Care Program here.",
			"mainLink" : "http://www.moore.org/programs/patient-care",
			"courtesy" : "Courtesy of Christopher Myers"
		},
		{
			"title"    : "Environmental Conservation Program",
			"subtitle" : "Long-Term Conservation of Brazil’s Tropical Forests",
			"icon"     : "Environmental-gordon-and-betty-moore-icon.gif",
			"src1"     : 'Lake-609-x-433.png',
			"src3"     : 'Lake-381-x-572.png',
			"src4"     : 'Lake-465-x-495.png',
			"impact"   : "Brazil’s Amazon Region Protected Areas (ARPA) program is the largest tropical forest conservation program in history. In 2014, an unprecedented collaboration among the Brazilian government, NGOs, and public and private funders (including the Moore Foundation), created an innovative financial model to finance the management and monitoring of ARPA territories in perpetuity. With $215 million, the plan for sustainable funding will ensure long-term protection of the world’s largest network of protected areas—in aggregate, nearly twice the size of California.",
			"grantees" : 'Grants related to ARPA include: <i><a target="_blank" href=\"https://www.moore.org/grants/list/GBMF183\"> Amazon Protected Area Project (ARPA) </a> </i> <i><a target="_blank" href=\"https://www.moore.org/grants/list/GBMF237"> ARPA Bridge Funding</a> </i> <i><a target="_blank" href=\"https://www.moore.org/grants/list/GBMF183.01"> Amazon Protected Area Project (ARPA) II </a> </i> <i><a target="_blank" href=\"https://www.moore.org/grants/list/GBMF3321"> Completing ARPA </a> </i> <i><a target="_blank" href=\"https://www.moore.org/grants/list/GBMF3322"> Completing ARPA </a> </i> <i><a target="_blank" href=\"https://www.moore.org/grants/list/GBMF3321.01"> Completing ARPA Phase II</a> </i> <i><a target="_blank" href=\"https://www.moore.org/grants/list/GBMF3322.01"> Completing ARPA Phase II</a> </i> <i><a target="_blank" href=\"https://www.moore.org/grants/list/GBMF4003"> ARPA for Life Fund Commitment </a> </i>',
			"moreLink" : "https://www.moore.org/article-detail?newsUrlName=$215-million-fund-to-protect-brazilian-amazon-rainforest-launched",
			"linkText" : "See more about our Environmental Conservation Program here.",
			"mainLink" : "https://www.moore.org/programs/environmental-conservation",
			"courtesy" : "Courtesy of Juan Pratginestos/WWF-Canon"
		},
		{
			"title"    : "San Francisco Bay Area Program",
			"subtitle" : "Acquisition of land near the Laurel Curve in Santa Cruz",
			"icon"     : "Environmental-gordon-and-betty-moore-icon.gif",
			"src1"     : firstSrcImageSize + "0005_3.png",
			"src3"     : secondSrcImageSize + "0005_3.png" ,
			"src4"     : thirdSrcImageSize + "0005_3.png",
			"impact"   : "In Santa Cruz County, Highway 17 severs the surrounding woodlands, creating a divide that has proven lethal to the area’s long-ranging wildlife. The foundation awarded a grant and a matching “program related investment” (a PRI supports charitable activities that involve the potential return of capital within an established time frame) to help acquire a keystone 280-acre property along the highway’s “Laurel Curve” for the construction of an under-crossing tunnel that will be vital to wildlife in Santa Cruz County, including the region’s mountain lions.",
			"grantees" : "<i><a target='_blank' href='http://www.landtrustsantacruz.org/'>Land Trust of Santa Cruz County</a></i>",
			"moreLink" : "https://www.moore.org/programs/san-francisco-bay-area/conservation",
			"linkText" : "See more about our San Francisco Bay Area Program here.",
			"mainLink" : "https://www.moore.org/programs/san-francisco-bay-area",
			"courtesy" : "Courtesy of the Land Trust of Santa Cruz County"
		},

		{
			"title"    : "Science Program",
			"icon"     : "Science-icon-gordon-and-betty.gif",
			"src1"     : firstSrcImageSize + "0000_8.png",
			"src3"     : secondSrcImageSize + "0000_8.png" ,
			"src4"     : thirdSrcImageSize + "0000_8.png",
			"subtitle" : "Competition to Reimagine the Chemistry Set",
			"impact"   : "The Moore Foundation, along with the Society for Science & the Public, announced winners in the SPARK (Science Play and Research Kit) Competition, a national prize competition to solicit ideas that reimagine the chemistry set for the 21st century.",
			"grantees" : "<i><a target='_blank' href=\'https://www.moore.org/docs/default-source/Grantee-Resources/spark-announce-press-release-final.pdf\'> SPARK Winners </a></i>",
			"moreLink" : "https://www.moore.org/article-detail?newsUrlName=winners-of-competition-to-reimagine-the-chemistry-set-announced",
			"linkText" : "See more about our Science Program here.",
			"mainLink" : "https://www.moore.org/programs/science",
			"courtesy" : "Courtesy of George Korir, Stanford University"
		},
		{
			"title"    : "Patient Care Program",
			"subtitle" : "UC Davis Medical Center Achieves Magnet Status",
			"icon"     : "Science-icon-gordon-and-betty.gif",
			"src1"     : firstSrcImageSize + "0003_5.png",
			"src3"     : secondSrcImageSize + "0003_5.png" ,
			"src4"     : thirdSrcImageSize + "0003_5.png",
			"impact"   : "Less than seven percent of hospitals in the nation receive the prestigious Magnet Recognition™ developed by the American Nurses Credentialing Center. Considered the highest recognition for nursing excellence, the Magnet designation is determined by nurses who recognize excellence in other nurses. UC Davis Medical Center joined the ranks of anointed clinical-care institutions in the country, becoming one of 25 organizations in California to receive the designation and the fifth organization in the state that the foundation has successfully helped reach Magnet status.",
			"grantees" : "<i><a target='_blank' href='http://www.ucdmc.ucdavis.edu/nursing/'>UC Davis Betty Irene Moore School of Nursing</a></i>",
			"moreLink" : "https://www.moore.org/initiative-strategy-detail?initiativeId=betty-irene-moore-school-of-nursing",
			"linkText" : "See more about our Patient Care Program here.",
			"mainLink" : "https://www.moore.org/programs/patient-care",
			"courtesy" : "Courtesy of Alexandra Worden"
		},
		{
			"title"    : "Environmental Conservation Program",
			"subtitle" : "Innovation in Salmon Aquaculture",
			"icon"     : "Environmental-gordon-and-betty-moore-icon.gif",
			"src1"     : firstSrcImageSize + "0002_6.png",
			"src3"     : secondSrcImageSize + "0002_6.png" ,
			"src4"     : thirdSrcImageSize + "0002_6.png",
			"impact"   : "For more than a decade, the foundation’s <a target='_blank' href=\"https://www.moore.org/programs/environmental-conservation/wild-salmon-ecosystems-initiative/\"> Wild Salmon Ecosystems Initiative </a> has been supporting work to develop more sustainable methods of salmon farming. In April 2014,the ‘Namgis First Nation’s Kuterra Project, which has created a land-based closed containment farm on Northern Vancouver Island, began selling their first harvest, soon averaging five metric tons each week. Within a few months, Kuterra’s salmon was winning awards from Monterey Bay Aquarium’s Seafood Watch, Vancouver Aquarium’s Ocean Wise and SeaChoice.",
			"grantees" : "<i><a target='_blank' href=\"http://tidescanada.org/programs/salmon-aquaculture-innovation-fund/\"> Tides Canada’s Salmon Aquaculture Innovation Fund </a> </i>",
			"moreLink" : "https://www.moore.org/programs/environmental-conservation/wild-salmon-ecosystems-initiative",
			"linkText" : "See more about our Environmental Conservation Program here.",
			"mainLink" : "https://www.moore.org/programs/environmental-conservation",
			"courtesy" : ""
		},
		{
			"title"    : "San Francisco Bay Area Program",
			"subtitle" : "Bay Area Observatory and Living Oceans",
			"icon"     : "San-Francisco-gordon-and-betty-moore-icon.gif",
			"src1"     : "Exploratorium-3.png",
			"src3"     : "Exploratorium-1.png",
			"src4"     : "Exploratorium-2.png",
			"impact"   : "In 2014, the <i><a target='_blank' href=\'https://www.moore.org/grants/list/GBMF4423'> Exploratorium brought exhibits and programming </a> </i> to life that are helping visitors better understand the biological, oceanographic, and atmospheric systems of San Francisco Bay and our global oceans. Grantees from our <i><a target='_blank' href=\'https://www.moore.org/programs/science/marine-microbiology-initiative'>science program’s marine microbiology initiative </a> </i> contributed research and data sets to help inform the exhibits and programs.",
			"grantees" : "<i>The<a target='_blank' href=\'http://www.exploratorium.edu\'> Exploratorium </a> </i>",
			"moreLink" : "https://www.moore.org/programs/san-francisco-bay-area/science-technology-museums",
			"linkText" : "See more about our Bay Area Program here.",
			"mainLink" : "https://www.moore.org/programs/san-francisco-bay-area",
			"courtesy" : "Courtesy of Amy Snyder and Exploratorium"
		}
	]
}

var contentQuotes = {
	"quotes" : [
		{ "quote"    : "\"Science and the type of rigorous inquiry that guides science are keys to achieving the outcomes we want.\" — Gordon and Betty Moore" },
		{ "quote"    : "\"Betty and I established the foundation because we believe it can make a significant and positive impact in the world.\"  — Gordon Moore" },
		{ "quote"    : "\"We want this not only for our children, but for our children’s children.\" — Betty Moore" },
		{ "quote"    : "\"We want the foundation to tackle large, important issues at a scale where it can achieve significant and measurable impacts.\"  — Gordon and Betty Moore" }
	]
}

function ProgramsViewModel() {
	var self = this;
	self.programs = ko.observableArray([]);

		var mappedPrograms = $.map(contentPrograms, function(item) {
			return item
		});
		self.programs(mappedPrograms);
}

function QuotesViewModel() {
	var self = this;
	self.quotes = ko.observableArray([]);

		var mappedQuotes = $.map(contentQuotes, function(item) {
			return item
		});
		self.quotes(mappedQuotes);
}

ko.applyBindings(new ProgramsViewModel(), document.getElementById('program'));

ko.applyBindings(new QuotesViewModel(), document.getElementById('inspiration'));
